# Atlas Theme

This is the repository for Atlas Theme based on Hubspot's CMS Boilerplate.

## Getting started

The HubSpot CMS Boilerplate is designed to work with both [local development](https://designers.hubspot.com/docs/tools/local-development) and the HubSpot Design Tools. Before getting started, you will need to have [Node.js](https://nodejs.org) installed and we strongly suggest that you set up a [HubSpot CMS Developer Sandbox](https://offers.hubspot.com/free-cms-developer-sandbox).

### Installing git

To manage the project source code we use git, a distributed version control for tracking changes. Download the lastest version of [git](https://git-scm.com/), in addition to that you should be familiar with following concepts: branches, commits, pull requests, staging area and merge conflicts...

Clone the respository:

`git clone git@bitbucket.org:kalungit/atlas-theme.git`

To setup ssh to be able to push changes without a password you will need to generate a ssh key pair. Check out this [article](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key) to learn how to do that.

If you are going to use ssh remember to set the remote url to point to this `git@bitbucket.org:kalungit/kalungi-theme.git`.

### Installing node and the dependencies

You also need [node](https://nodejs.org/en/) to run the cli tools that are used within the project, once you have node it should come with `npm`. Because I have been using [yarn](https://yarnpkg.com/getting-started/install) on the project and not npm you will need to install it:

```
npm install -g yarn
```

To install all the project dependencies you have to go to the project directory and run `yarn install`.

Once you have all the dependencies installed run `npx hs init` to create a `hubspot.config.yml` file that will store the credentials of a hubspot portal, you also must name it `PROD`. I recommend you to use your own developer account which you can create using this [link](https://developers.hubspot.com/get-started).

Once everything is set up you should be able to run the following commands:

The `yarn run build`, which compiles all the `scss` files into `css` files and creates a file called `main.css` that includes all the compiled css files.

The `yarn run watch`, which does the same thing as the `build` command but it starts a watch process that uploads the changes that you make on hubspot automatically.

Refer to the [hubspot cli documentation](https://developers.hubspot.com/docs/cms/developer-reference/local-development-cms-cli), for more information of what you can do with it.

### Setup the respository deployments

Enable pipelines on the respository and make sure the `bitbucket-pipelines.yml` file references existing deployments, for each deployment add a `HUBSPOT_API_KEY` and a `HUBSPOT_PORTAL_ID` variables (Don't forget to secure the API key).

## Working with custom modules

To create a new custom module the easiest way would be from the hubspot cms interface, be sure to link the `main.css` file to it and add all the fields that you are going to use.  

When you are done you can use the `npx hs fetch` command to fetch the module to your local project.

Hubspot modules are folders with the following files:

- `fields.json`: This file stores all the information about the fields of the module.
- `meta.json`: This file has all the metada of the module.
- `module.css`: The css of the module
- `module.html`: The html of the module (You can use hubl inside this)
- `module.js`: The javascript of the module

For now we are not using the `module.css` file, instead we are creating a specific file with the name of the module inside the `scss` folder to put all the styles of that module there, that way we can share styles between modules and avoid duplicated code.

For class names you should try to use the [BEM](http://getbem.com/naming/) naming convention.

Notes:

- The fetch the data from the fields inside the html you have to use the `module` object.
- You can't use hubl inside the `module.js` and `module.css`. You have to use the `require_js` and `require_css` tags

### Getting started with sass

For this workflow we use `sass`, to get started I recommend you to check out the [guide](https://sass-lang.com/guide).

It is also worth mention that we have two function to inject hubl inside the compiled css files, those function are `hubl` and `color`.

- The `hubl` function basically render a string inside a double curly brackets.
- The `color` function renders valid rgba color from a theme field.

Normally you shouldn't use these functions outside of the settings folder, instead you should define variables with names that make sense to use them in the other files.

## Getting started on coded templates

You should be familiar with hubl tags to understand better coded templates. That is because `dnd_areas` are hubl tags.

Normally every template extens the `base.html` template and overwrite the body block where you place the drag and drop areas for the user to put content.

If you need to overwrite the header, and change the navbar for just one template then you could overwrite the header block. The same applies with the footer block.

Notice that the header and the footer are global partials, those are very similar to global groups. This means that if then content is changed it will be reflected on every page that uses that partial.

To include a module inside a drag and drop area in a coded template you just need to use the `dnd_module` tag and specify a **relative** path to that module.
