const { src, dest, lastRun, watch, parallel, series } = require('gulp');
const { exec } = require('child_process');
const path = require('path');
const fs = require('fs');
const yaml = require('js-yaml');
const { Transform } = require('stream');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const Vinyl = require('vinyl');

const PORTAL_ID = process.env.HUBSPOT_PORTAL_ID;
const API_KEY = process.env.HUBSPOT_API_KEY;

const THEME_PATH = process.env.THEME_PATH || 'meme-theme';

/* ITCSS Directory priority */
const STYLE_DIRS = [
  'imports',
  'generic',
  'objects',
  'elements',
  'components',
  'templates',
];

const SCSS_GLOBS = STYLE_DIRS.map((d) => `src/scss/${d}/**/*.scss`);
const CSS_GLOBS = STYLE_DIRS.map((d) => `src/css/${d}/**/*.css`);

function template(filename) {
  const stream = new Transform({ objectMode: true });
  let content = '';

  stream._transform = (file, enc, cb) => {
    content += `{% include './${file.relative.replace('\\', '/')}' %}\n`;
    cb(null);
  };

  stream._final = (cb) => {
    const result = new Vinyl({
      path: filename,
      contents: new Buffer.from(content),
    });
    stream.push(result);
    cb();
  };

  return stream;
}

function compile() {
  return src(SCSS_GLOBS, { base: 'src/scss', since: lastRun(compile) })
    .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
    .pipe(
      rename((path) => ({
        dirname: path.dirname,
        basename: '_' + path.basename,
        extname: '.css',
      }))
    )
    .pipe(dest('src/css'));
}

function bundle() {
  return src(CSS_GLOBS, { base: 'src/css' })
    .pipe(template('main.css'))
    .pipe(dest('src/css'));
}

const commandFactory = (command) => () => {
  /* Uses default portal */
  const child = exec(command);

  child.stdout.on('data', (data) => process.stdout.write(data));
  child.stderr.on('data', (data) => process.stdout.write(data));

  return child;
};

const HubspotWatch = commandFactory(`npx hs watch src '${THEME_PATH}' --remove`);
const HubspotUpload = commandFactory(`npx hs upload src '${THEME_PATH}'`);
const HubspotFetch = commandFactory(`npx hs fetch '${THEME_PATH}' src`);

const Build = series(compile, bundle);

const Watch = function () {
  watch('src/scss/**/*.scss', Build);
};

const makeConfig = (cb) => {
  if (fs.existsSync('hubspot.config.yml')) {
    cb(new Error('hubspot.config.yml was already created!'));
  }

  const portalConfig = {
    name: 'PROD',
    useRawAssets: true,
    portalId: PORTAL_ID,
    apiKey: API_KEY,
  };

  const config = {
    defaultPortal: 'PROD',
    portals: [portalConfig],
  };

  fs.writeFile(
    path.join(process.cwd(), 'hubspot.config.yml'),
    yaml.safeDump(config),
    cb
  );
};

/* Change tasks display names */

HubspotWatch.displayName = 'hubspot-watch';
Watch.displayName = 'scss-watch';

exports.makeconf = makeConfig;
exports.watch = series(Build, parallel(Watch, HubspotWatch));
exports.deploy = HubspotUpload;
exports.fetch = HubspotFetch;
exports.compile = compile;
exports.build = Build;
exports.default = Build;
